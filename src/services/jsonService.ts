import {Service} from "typedi";
import {Request, Response} from "express";
import {WebSocketServer} from 'ws';

const path = require('path');
import fs from 'fs';
import {Subject} from "rxjs";

@Service()
export class JsonService {

  wss = new WebSocketServer({port: 8082, path: '/all_data'});
  allData$ = new Subject<any>();

  constructor() {
    this.mainSocket().then(r => r);
  }

  public async getAllData(req: Request, res: Response) {
    try {
      let data: { message: any, flag: boolean } = this.readData();
      if (!data.flag) {
        return {message: "No data found.", flag: false};
      }
      console.log("DATA ", data.message);
      return {message: data.message, flag: true};
    } catch (e) {
      return {message: e.message, flag: false};
    }
  }

  public async updateData(req: Request, res: Response) {
    try {
      let data: { message: any, flag: boolean } = this.readData();
      if (!data.flag) {
        return {message: "No data found.", flag: false};
      }
      if (req.body.isParent) {
        let parentKey = req.body.data.parent;
        let parentIndex = data.message.findIndex(it => it['EmployeeID'].toLowerCase() === parentKey);
        for(let i=0;i<req.body.data.child.length;i++) {
          let val = req.body.data.child[i];
          data.message[parentIndex][val['key']] = val['value'];
        }
      } else {
        for (let i = 0; i < req.body.data.length; i++) {
          let parentKey = req.body.data[i].parent;
          let parentIndex = data.message.findIndex(it => it['EmployeeID'].toLowerCase() === parentKey);

          for (let j = 0; j < req.body.data[i].child.length; j++) {
            let val = req.body.data[i].child[j];
            if (val['key'] !== "isParent") {
              let child = data.message[parentIndex].Children;
              let childIndex = child.findIndex(it => it['EmployeeID'].toLowerCase() === val['id'].toString());
              child[childIndex][val['key']] = val['value'];
            }
          }
        }
      }
      let writeData: { message: boolean | string, flag: boolean } = this.writeData(data.message);
      if (!writeData.flag) {
        return {message: writeData.message, flag: false};
      }
      this.allData$.next(data.message);
      return {message: data.message, flag: true};
    } catch (e) {
      return {message: e.message, flag: false};
    }
  }

  public async deleteData(req: Request, res: Response) {
    try {
      let data: { message: any, flag: boolean } = this.readData();
      if (!data.flag) {
        return {message: "No data found.", flag: false};
      }
      if (req.body.isParent) {
        let index = data.message.findIndex(it => it['EmployeeID'].toLowerCase() === req.body.data);
        data.message.splice(index, 1);
      } else {
        for (let i = 0; i < req.body.data.length; i++) {
          let parentKey = req.body.data[i].parent;
          let parentIndex = data.message.findIndex(it => it['EmployeeID'].toLowerCase() === parentKey);
          for (let j = 0; j < req.body.data[i].child.length; j++) {
            let val = req.body.data[i].child[j];
            let child = data.message[parentIndex].Children;
            let childIndex = child.findIndex(it => it['EmployeeID'].toLowerCase() === val.toString());
            child.splice(childIndex, 1);
          }
        }
      }
      let writeData: { message: boolean | string, flag: boolean } = this.writeData(data.message);
      if (!writeData.flag) {
        return {message: writeData.message, flag: false};
      }
      this.allData$.next(data.message);
      return {message: data.message, flag: true};
    } catch (e) {
      return {message: e.message, flag: false};
    }
  }

  public readData(): { message: any, flag: boolean } {
    try {
      const jsonData = fs.readFileSync(path.resolve('files/data.json'));
      return {message: JSON.parse(jsonData.toString()), flag: true};
    } catch (e) {
      return {message: e.message, flag: false};
    }
  }

  public writeData(data: any): { message: boolean | string, flag: boolean } {
    try {
      const stringifyData = JSON.stringify(data);
      fs.writeFileSync(path.resolve('files/data.json'), stringifyData);
      return {message: true, flag: true};
    } catch (e) {
      return {message: e.message, flag: false};
    }
  }

  public async mainSocket() {
    try {
      this.wss.on('connection', async (ws) => {

        this.allData$.subscribe((it) => {
          ws.send(JSON.stringify(it));
        });
      });
    } catch (e) {
      console.log(e.message);
    }
  }

}
