import {Request, Response, Router} from "express";
import {Container} from "typedi";
import {JsonService} from "@/services/jsonService";
import {celebrate} from "celebrate";
import Joi from "joi";

const route = Router();

export default (app: Router) => {

  app.use('/json', route);

  route.get('/get_data', async function (req: Request, res: Response) {
    const coinsData: { message: any, flag: boolean } = await Container.get(JsonService)
      .getAllData(req, res);
    if (coinsData.flag) {
      return res.status(200).json({success: true, result: {message: coinsData.message}});
    } else {
      return res.status(200).json({success: false, result: {error: coinsData.message}});
    }
  });

  route.patch('/update_data', celebrate({
    body: Joi.object().keys({
      data: Joi.alternatives().conditional('isParent', {
        is: false,
        then: Joi.array().items({
          parent: Joi.string().required().lowercase(),
          child: Joi.array().items({
            id: Joi.string().required().lowercase(),
            key: Joi.string().required(),
            value: Joi.string().required()
          })
        }),
        otherwise: Joi.object().keys({
          parent: Joi.string().required().lowercase(),
          child: Joi.array().items({
            id: Joi.string().required().lowercase(),
            key: Joi.string().required(),
            value: Joi.string().required()
          })
        }),
      }),
      isParent: Joi.boolean().required(),
    }),
  }), async function (req: Request, res: Response) {
    const coinsData: { message: any, flag: boolean } = await Container.get(JsonService)
      .updateData(req, res);
    if (coinsData.flag) {
      return res.status(200).json({success: true, result: {message: coinsData.message}});
    } else {
      return res.status(200).json({success: false, result: {error: coinsData.message}});
    }
  });

  route.post('/delete_data', celebrate({
    body: Joi.object().keys({
      data: Joi.alternatives().conditional('isParent', {
        is: false,
        then: Joi.array().items({
          parent: Joi.string().required().lowercase(),
          child: Joi.array().items(
            Joi.string().required().lowercase(),
          ),
        }),
        otherwise: Joi.string().required().lowercase(),
      }),
      isParent: Joi.boolean().required(),
    }),
  }), async function (req: Request, res: Response) {
    const coinsData: { message: any, flag: boolean } = await Container.get(JsonService)
      .deleteData(req, res);
    if (coinsData.flag) {
      return res.status(200).json({success: true, result: {message: coinsData.message}});
    } else {
      return res.status(200).json({success: false, result: {error: coinsData.message}});
    }
  });


}
